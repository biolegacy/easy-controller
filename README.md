This is an easy controller for Unity. It should help you out for rapid prototyping since you can change the controller in real-time.
Currently, it has `Top down` and `Third-person` controller.

## Keep in mind
```
This asset is currently in development.
```

#Top down

Basic top down controller which makes the player face at the player's moving direction. 
Also there is an option to face the player at the mouse direction.

#Third person controller

Basic third person controller. Basic `Camera collision` is implemented too.