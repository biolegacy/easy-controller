﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
    #region Public variables
    public PlayerMovement controls;
    #endregion

    #region Private variable
    PlayerMotor motor;
    CameraFollow cam;
    #endregion

    private void Reset()
    {
        motor = GetComponent<PlayerMotor>();
    }
    private void Awake()
    {
        controls = new PlayerMovement();
        controls.Player.Movement.performed += ctx => Move(ctx.ReadValue<Vector2>());
        controls.Player.Movement.canceled += ctx => Move(Vector2.zero);
        controls.Player.MousePosition.performed += ctx => MousePosition(ctx.ReadValue<Vector2>());
        controls.Player.MouseMovent.performed += ctx => MouseMovement(ctx.ReadValue<Vector2>());
        controls.Player.MouseMovent.canceled += ctx => MouseMovement(Vector2.zero);
    }

    private void Start()
    {
        if (motor == null)
            motor = GetComponent<PlayerMotor>();
    }

    #region Private methods
    public void Move(Vector2 action) {
        motor.Move(action);
    }

    public void MousePosition(Vector2 m_position) {
        motor.MousePosition(m_position);
    }

    public void MouseMovement(Vector2 m_movement) {
        Camera.main.gameObject.GetComponent<CameraFollow>().Rotate(m_movement);
    }
    public void OnEnable()
    {
        controls.Enable();
    }

    public void OnDisable()
    {
        controls.Disable();
    }
    #endregion
}
