﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CameraFollow : MonoBehaviour
{
    #region Public variables
    // General shared parameters. ( Not necessarely relative to all 3 types of camera. Sometimes only relative to 2 of them )
    [SerializeField] GameObject target;
    public Vector3 offset = Vector3.zero;
    // Top down specific parameters
    public Vector2 heightLimitation = Vector2.zero;
    public float dampTime = 0f;
    // Third person
    public Vector2 pitchMinMax = Vector2.zero;
    public float distance = 0f;
    public LayerMask whatIsEnvironment;
    public bool DebugRay;
    #endregion

    #region Private variables
    // Private variables
    Vector3 velocity = Vector3.zero;
    EasyController easyController;

    // Third person camera specific variables
    Vector3 targetRotation = Vector3.zero;
    float Yaw;
    float Pitch;
    float sensitivity;
    float adjustedDistance;
    bool invert;
    bool MMORPGCamera;

    Ray CamRay;
    RaycastHit hit;

    // Camera collision
    [SerializeField] int rayGridX, rayGridY;
    Vector3[] camClip, clipDirection, playerClip, rayColOrigin, rayColPoint;
    bool[] rayColHit;

    // Editor speicific variable
    [HideInInspector] public ControllerType currentController;
    #endregion

    private void Awake()
    {
    }
    // Start is called before the first frame update
    void Start()
    {
        adjustedDistance = distance;
        CameraClipInfo();
    }

    // Update is called once per frame
    void Update()
    {
        //  if(easyController.GetControllerType() == ControllerType.ThirdPerson)
        if (rayGridX * rayGridY != rayColHit.Length)
            CameraClipInfo();
    }

    private void FixedUpdate()
    {
        //PerformSmoothFollow();
    }

    private void LateUpdate()
    {
        switch (currentController) {
            case ControllerType.TopDown:
                PerformSmoothFollowTopdown();
                break;
            case ControllerType.ThirdPerson:
                PerformThirdPersonBehavior();
                break;
        }
    }

    #region Public methods
    public void SetTarget(GameObject value) {
        target = value;
    }
    public void SetEasyController(EasyController _easyController) {
        easyController = _easyController;
    }

    public void SetControllerType(ControllerType newType) {
        currentController = newType;
    }

    public void Rotate(Vector2 rotation) {
        Yaw += rotation.x * sensitivity * Time.deltaTime;
        Pitch -= rotation.y * sensitivity * ( invert ? -1 : 1) * Time.deltaTime;
        Pitch = Mathf.Clamp(Pitch, pitchMinMax.x, pitchMinMax.y);
    }

    public void SetValuesThirdPerson(float _sensitivity, bool _MMORPGCamera, bool _invert)
    {
        MMORPGCamera = _MMORPGCamera;
        invert = _invert;
        sensitivity = _sensitivity;
    }

    public void SetMMORPGToggle(bool value) {
        MMORPGCamera = value;
    }

    public void SetInvertMouse(bool value) {
        invert = value;
    }

    public void SetSensitivity(float value) {
        sensitivity = value;
    }
    #endregion

    #region Private methods
    void PerformSmoothFollowTopdown() 
    {
        Vector3 desiredPos = target.transform.position + offset;
        desiredPos.y = Mathf.Clamp(desiredPos.y, heightLimitation.x, heightLimitation.y);
        transform.position = Vector3.SmoothDamp(transform.position, desiredPos, ref velocity, dampTime);

        transform.LookAt(target.transform, target.transform.up);
    }

    void PerformThirdPersonBehavior() {
        targetRotation = new Vector3(Pitch, Yaw);

        CameraCollision();

        transform.eulerAngles = targetRotation;
        transform.position = target.transform.position - transform.forward * adjustedDistance;
    }

    void CameraCollision() {
        /**
         * Advanced camera collision
         * I followed a tutorial to do this camera collision system. You can find the originial tutorial at the following links.
         * Do not forget to like and subscribe to support the generous person who made this tutorials for us.
         * Tutorial link: 
         * Part 1: [https://www.youtube.com/watch?v=fdBjK-YTGIQ]
         * Part 2: [https://www.youtube.com/watch?v=Rohul_FU1vQ&t=228s] 
        */
        for (int i = 0; i < camClip.Length; i++) 
        {
            Vector3 clipPoint = Camera.main.transform.up * camClip[i].y + Camera.main.transform.right * camClip[i].x + Camera.main.transform.forward * camClip[i].z;
            clipPoint += target.transform.position - (Camera.main.transform.forward * (distance + Camera.main.nearClipPlane));

            Vector3 playerPoint = Camera.main.transform.up * camClip[i].y + Camera.main.transform.right * camClip[i].x;
            playerPoint += target.transform.position;

            clipDirection[i] = (clipPoint - playerPoint).normalized;
            playerClip[i] = playerPoint;
        }

        int currentRay = 0;
        bool isColliding = false;

        float rayX = rayGridX - 1;
        float rayY = rayGridY - 1;

        for (int x = 0; x < rayGridX; x++)  {

            Vector3 ClipUpperPoint = Vector3.Lerp(clipDirection[1], clipDirection[2], x / rayX);
            Vector3 ClipLowerPoint = Vector3.Lerp(clipDirection[0], clipDirection[3], x / rayX);

            Vector3 PlayerUpperPoint = Vector3.Lerp(playerClip[1], playerClip[2], x / rayX);
            Vector3 PlayerLowerPoint = Vector3.Lerp(playerClip[0], playerClip[3], x / rayX);
            for (int y = 0; y < rayGridY; y++) {
                CamRay.origin = Vector3.Lerp(PlayerUpperPoint, PlayerLowerPoint, y / rayY);
                CamRay.direction = Vector3.Lerp(ClipUpperPoint, ClipLowerPoint, y / rayY);
                rayColOrigin[currentRay] = CamRay.origin;

                if (Physics.Raycast(CamRay, out hit, distance, whatIsEnvironment))
                {
                    isColliding = true;
                    rayColHit[currentRay] = true;
                    rayColPoint[currentRay] = hit.point;

                    if(DebugRay)
                    {
                        Debug.DrawLine(CamRay.origin, hit.point, Color.cyan);
                        Debug.DrawLine(hit.point, CamRay.origin + CamRay.direction * distance, Color.magenta);
                    }
                }
                else
                {
                    rayColHit[currentRay] = false;

                    if(DebugRay && hit.collider == null)
                        Debug.DrawLine(CamRay.origin, CamRay.origin + CamRay.direction * distance, Color.cyan);
                }
                currentRay++;
            }
        }

        if (isColliding)
        {
            float minRayDistance = float.MaxValue;
            currentRay = 0;

            for (int i = 0; i < rayColHit.Length; i++) {
                if (rayColHit[i]) {
                    float colDistance = Vector3.Distance(rayColOrigin[i], rayColPoint[i]);

                    if (colDistance < minRayDistance) {
                        minRayDistance = colDistance;
                        currentRay = i;
                    }
                }
            }

            // Setting the camera distance based on the lowest ray distance
            Vector3 clipCenter = target.transform.position - (Camera.main.transform.forward * distance);

            adjustedDistance = Vector3.Dot(-Camera.main.transform.forward, clipCenter - rayColPoint[currentRay]);
            adjustedDistance = distance - (adjustedDistance + 0.3f);
            adjustedDistance = Mathf.Clamp(adjustedDistance, 0, distance);
        }
        else 
            adjustedDistance = distance;
        

        /* Basic Camera collision for reference
        // Set the ray origin and target
        CamRay.origin = target.transform.position;
        CamRay.direction = -transform.forward;

        if (DebugRay)
            Debug.DrawRay(target.transform.position, -transform.forward * Vector3.Distance(transform.position, target.transform.position), Color.cyan);

        if (Physics.Raycast(CamRay, out hit, distance, whatIsEnvironment))
        {
            if (!isColliding)
                isColliding = true;
            adjustedDistance = hit.distance;
        }
        else {
            if(hit.collider == null)
                adjustedDistance = distance;
        }
        */
    }

    void CameraClipInfo() {
        camClip = new Vector3[4];

        Camera.main.CalculateFrustumCorners(new Rect(0, 0, 1, 1), Camera.main.nearClipPlane, Camera.MonoOrStereoscopicEye.Mono, camClip);

        clipDirection = new Vector3[4];
        playerClip = new Vector3[4];

        int rays = rayGridX * rayGridY;

        rayColOrigin = new Vector3[rays];
        rayColPoint = new Vector3[rays];
        rayColHit = new bool[rays];
    }

    #endregion
}

public enum CameraType { 
    TopDown,
    ThirdPerson,
    FirstPerson
}