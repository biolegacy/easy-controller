﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour
{
    #region Public variables
    [HideInInspector] new public Rigidbody rigidbody;
    #endregion
    #region Private variables
    Vector3 velocity = Vector3.zero;
    Vector3 desiredMoveDirection = Vector3.zero;
    Vector2 mousePosition = Vector2.zero; 
    [HideInInspector, SerializeField] bool lookAtMouse = false;
    [HideInInspector] float movespeed = 0f;
    [HideInInspector] float dampTime = 0f;
    EasyController easyController;
    #endregion
    private void Reset()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
        if (rigidbody != null)
            rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        easyController = GetComponent<EasyController>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void FixedUpdate()
    {
        if (rigidbody != null) {
            // Perform the following functions if the player game object has rigidbody component
            switch (easyController.GetControllerType()) {
                case ControllerType.TopDown:
                    {
                        HandleMovementTopdown();
                        HandlenRotationTopdown();
                        break;
                    }
                case ControllerType.ThirdPerson: {
                        HandleMovementThirdperson();
                        HandleRotationThirdperson();
                        break;
                    }
            }
            /*
            PerformMove();
            PerformRotation();
            */
        }
    }

    #region Public methods
    public void Move(Vector2 _velocity) 
    {
        if (_velocity != Vector2.zero)
            velocity = new Vector3(_velocity.x, 0, _velocity.y);
        else velocity = Vector3.zero;
    }

    public void MousePosition(Vector2 m_position) {
        mousePosition = m_position;
    }

    public void SetValuesTopdown(float _movespeed, float _dampTime, bool _lookAtMouse) {
        movespeed = _movespeed;
        dampTime = _dampTime;
        lookAtMouse = _lookAtMouse;
    }

    public void SetMovespeed(float _movespeed) {
        movespeed = _movespeed;
    }
    #endregion

    #region Private methods
    void PerformMove() {
        switch (easyController.GetControllerType()) {
            case ControllerType.TopDown: {
                    HandleMovementTopdown();
                    break;
                }
            case ControllerType.ThirdPerson: {
                    HandleMovementThirdperson();
                    break;
                }
        }
    }

    void PerformRotation() 
    {
        HandlenRotationTopdown();
    }

    void AimTopdown() 
    {
        RaycastHit hit;
        Plane plane = new Plane(Vector3.up, new Vector3(0f, transform.position.y, 0f));
        float rayLength;

        Ray ray = Camera.main.ScreenPointToRay(mousePosition);

        if (Physics.Raycast(ray, out hit, 100f))
        {
            transform.LookAt(hit.point);

            transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f);
        } else if (plane.Raycast(ray, out rayLength)) 
        {

            Vector3 targetPos = ray.GetPoint(rayLength);

            transform.LookAt(new Vector3(targetPos.x, transform.position.y, targetPos.z), Vector3.up);
            transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f);
        }

    }

    // Top down movement and rotation handler
    void HandleMovementTopdown() 
    {
        if (velocity != Vector3.zero)
        {
            rigidbody.MovePosition(rigidbody.position + (velocity * Time.deltaTime * movespeed));
        }
    }

    void HandlenRotationTopdown() {
        if (lookAtMouse == false && velocity != Vector3.zero)
        {
            // Face toward
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(velocity.normalized), dampTime);
        }
        else if(lookAtMouse)
        {
            AimTopdown();
        }
    }

    // 3rd person movement and rotation handler
    void HandleMovementThirdperson() {
        if (velocity != Vector3.zero) {
            Vector3 forward;
            Vector3 right;

            forward = transform.forward;
            right = transform.right;

            desiredMoveDirection = (forward * velocity.z + right * velocity.x);
            if (desiredMoveDirection.magnitude > 1)
                desiredMoveDirection.Normalize();

            // Move the character
            rigidbody.MovePosition(rigidbody.position + (desiredMoveDirection * movespeed * Time.deltaTime));

        }
    }

    void HandleRotationThirdperson() {
        if (velocity != Vector3.zero)
        {
            Vector3 forward = Camera.main.transform.forward;
            forward.y = 0f;
            // Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(forward), 0.125f);
            transform.rotation = Quaternion.LookRotation(forward); 
        }
    }
    #endregion
}
