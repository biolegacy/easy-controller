﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CameraFollow))]
[CanEditMultipleObjects]
public class CameraFollowEditor : Editor
{
    #region Private variables
    CameraFollow cameraFollow;
    [SerializeField] SerializedProperty targetPlayer;
    [SerializeField] SerializedProperty whatIsEnvironment;
    [SerializeField] SerializedProperty debugRay;
    [SerializeField] SerializedProperty rayGridX;
    [SerializeField] SerializedProperty rayGridY;
    #endregion
    private void OnEnable()
    {
        targetPlayer = serializedObject.FindProperty("target");
        whatIsEnvironment = serializedObject.FindProperty("whatIsEnvironment");
        debugRay = serializedObject.FindProperty("DebugRay");
        rayGridX = serializedObject.FindProperty("rayGridX");
        rayGridY = serializedObject.FindProperty("rayGridY");
        cameraFollow = (CameraFollow) target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        /**
         * Shared variables
         */
        GUILayout.Label("Shared variables", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(this.targetPlayer);

        /**
         * Camera specific variables 
         */
        if (cameraFollow.currentController == ControllerType.TopDown)
        {
            GUILayout.Label("Top down camera", EditorStyles.boldLabel);
            cameraFollow.offset = EditorGUILayout.Vector3Field("Camera Offset: ", cameraFollow.offset);
            cameraFollow.heightLimitation = EditorGUILayout.Vector2Field("Height Limitation: ", cameraFollow.heightLimitation);
            cameraFollow.dampTime = EditorGUILayout.FloatField("Damp time: ", cameraFollow.dampTime);
        }
        else if (cameraFollow.currentController == ControllerType.ThirdPerson) {
            GUILayout.Label("Third person camera", EditorStyles.boldLabel);
            cameraFollow.pitchMinMax = EditorGUILayout.Vector2Field("Pitch min and max: ", cameraFollow.pitchMinMax);
            cameraFollow.distance = EditorGUILayout.FloatField("Camera distance: ", cameraFollow.distance);
            EditorGUILayout.PropertyField(this.debugRay);
            EditorGUILayout.PropertyField(this.whatIsEnvironment);
            EditorGUILayout.PropertyField(this.rayGridX);
            EditorGUILayout.PropertyField(this.rayGridY);
        }
        serializedObject.ApplyModifiedProperties();
    }
}