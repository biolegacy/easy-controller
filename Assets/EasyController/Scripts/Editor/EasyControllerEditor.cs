﻿using System;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EasyController))]
[CanEditMultipleObjects]
public class EasyControllerEditor : Editor
{
    // SerializedProperty easyController;
    #region Private variabes
    [SerializeField] SerializedProperty movespeed;
    [SerializeField] SerializedProperty turnspeed;
    [SerializeField] SerializedProperty controllerType;
    [SerializeField] SerializedProperty lookAtMouse;
    [SerializeField] SerializedProperty MMORPG_Camera;
    [SerializeField] SerializedProperty invert;
    EasyController easyController;
    PlayerMotor motor;
    CameraFollow cameraFollow;

    #endregion
    private void OnEnable()
    {
        if (Camera.main.GetComponent<CameraFollow>() != null) {
            cameraFollow = Camera.main.GetComponent<CameraFollow>();
        }
        movespeed = serializedObject.FindProperty("movespeed");
        turnspeed = serializedObject.FindProperty("turnspeed");
        controllerType = serializedObject.FindProperty("controllerType");
        lookAtMouse = serializedObject.FindProperty("lookAtMouse");
        MMORPG_Camera = serializedObject.FindProperty("MMORPG_Camera");
        invert = serializedObject.FindProperty("invert");
        easyController = (EasyController) target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        GUILayout.Space(20f);
        GUILayout.Label("Easy Character Controller", EditorStyles.boldLabel);
        GUILayout.Label("Easy character controller gives you necessary basic functionality for character controller. Easy character controller designed in a way to developer to easily extend the existing controller to get the controller (functionality/behavior) the developer visualizes", EditorStyles.helpBox);

        GUILayout.Space(10f);
        GUILayout.Label("Common values config", EditorStyles.boldLabel);

        // Base movespeed setter section
        GUILayout.Space(10f);
        GUILayout.BeginHorizontal();
        EditorGUILayout.PropertyField(this.movespeed);
        // easyController.movespeed = EditorGUILayout.FloatField("Movespeed: ", easyController.movespeed);
        GUILayout.EndHorizontal();

        // Base turnspeed setter section
        switch (easyController.controllerType)
        {
            // Aiming option
            case ControllerType.TopDown:
                {
                    GUILayout.BeginHorizontal();
                    easyController.damptime = EditorGUILayout.FloatField("Turnspeed: ", easyController.damptime);
                    GUILayout.EndHorizontal();
                    break;
                }
            case ControllerType.ThirdPerson:
                {
                    GUILayout.BeginHorizontal();
                    easyController.sensitivity = EditorGUILayout.FloatField("Mouse sensitivity: ", easyController.sensitivity);
                    cameraFollow.SetSensitivity(easyController.sensitivity);
                    GUILayout.EndHorizontal();
                    break;
                }
        }
        // Controller type selection
        GUILayout.Space(20);
        GUILayout.Label("Easy Character Controller selection", EditorStyles.boldLabel);
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        // EditorGUILayout.PropertyField(this.controllerType);
        easyController.controllerType = (ControllerType)EditorGUILayout.EnumPopup("Controller type: ", easyController.controllerType);
        cameraFollow.SetControllerType(easyController.controllerType);
        GUILayout.EndHorizontal();
        // Camera settings
        GUILayout.Space(20);
        GUILayout.Label("Camera base config", EditorStyles.boldLabel);
        GUILayout.Space(10f);
        GUILayout.Label("Aiming behavior", EditorStyles.boldLabel);
        switch(easyController.controllerType)
        {
            // Aiming option
            case ControllerType.TopDown:
                {
                    EditorGUILayout.PropertyField(this.lookAtMouse);
                    break;
                }
            case ControllerType.ThirdPerson: 
                {
                    //EditorGUILayout.PropertyField(this.MMORPG_Camera);
                    // EditorGUILayout.PropertyField(this.invert);
                    easyController.MMORPG_Camera = EditorGUILayout.Toggle("MMORPG: ", easyController.MMORPG_Camera);
                    cameraFollow.SetMMORPGToggle(easyController.MMORPG_Camera);
                    easyController.invert = EditorGUILayout.Toggle("Invert mouse: ", easyController.invert);
                    cameraFollow.SetInvertMouse(easyController.invert);
                    break;
                }
        }
        /*
        GUILayout.Space(25);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            Debug.Log("Player movespeed has been saved!");
        }

        if (GUILayout.Button("Reset"))
        {
            PlayerPrefs.DeleteAll();

            Debug.Log("Player prefs has been resetted");
        }
        GUILayout.EndHorizontal();
        */
        serializedObject.ApplyModifiedProperties();
    }
}
