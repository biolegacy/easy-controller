﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable, RequireComponent(typeof(PlayerController))]
public class EasyController : MonoBehaviour
{
    #region Private variables
    PlayerMotor motor;
    public ControllerType controllerType = ControllerType.TopDown;
    [SerializeField] public float movespeed;
    [SerializeField] public float damptime;
    [SerializeField] public float sensitivity;
    [SerializeField] public bool lookAtMouse;
    [SerializeField] public bool MMORPG_Camera;
    [SerializeField] public bool invert;
    #endregion
    #region Private variables
    float currentMovespeed;
    float currentDampTime;
    bool currentLookAtMouse;
    bool currentMMORPGCamera;
    bool currentInvert;
    float currentSensitivy;
    #endregion

    private void Reset()
    {
        if (Camera.main.GetComponent<CameraFollow>() == null) {
            Camera.main.gameObject.AddComponent<CameraFollow>();
            Camera.main.GetComponent<CameraFollow>().SetTarget(this.gameObject);
        }
    }

    private void Start()
    {
        motor = GetComponent<PlayerMotor>();
        switch (controllerType) {
            case ControllerType.TopDown:
                {
                    SetCurrentValuesTopdown(movespeed, damptime, lookAtMouse);
                    motor.SetValuesTopdown(movespeed, damptime, lookAtMouse);
                    break;
                }
            case ControllerType.ThirdPerson:
                {
                    Camera.main.GetComponent<CameraFollow>().SetEasyController(this);
                    motor.SetMovespeed(movespeed);
                    Camera.main.GetComponent<CameraFollow>().SetValuesThirdPerson(sensitivity, MMORPG_Camera, invert);
                    break;
                }
        }
    }
    private void Update()
    {
        CheckIfAnyValueIsUpdated();
    }

    #region Private methods
    void CheckIfAnyValueIsUpdated() {
        switch (controllerType) {
            case ControllerType.TopDown:
                {
                    if (currentMovespeed != movespeed || currentDampTime != damptime || currentLookAtMouse != lookAtMouse)
                    {
                        motor.SetValuesTopdown(movespeed, damptime, lookAtMouse);
                        SetCurrentValuesTopdown(movespeed, damptime, lookAtMouse);
                    }
                    break;
                }
            case ControllerType.ThirdPerson:
                {
                    if (currentMovespeed != movespeed || currentSensitivy != sensitivity || currentMMORPGCamera != MMORPG_Camera || currentInvert != invert) {
                        motor.SetMovespeed(movespeed);
                        Camera.main.GetComponent<CameraFollow>().SetValuesThirdPerson(sensitivity, MMORPG_Camera, invert);
                        SetCurrentValuesThirdPerson(sensitivity, MMORPG_Camera, invert);
                    }
                    break;
                }
        }
        
    }

    void SetCurrentValuesTopdown(float _movespeed, float _damptime, bool _lookAtMouse) {
        currentMovespeed = _movespeed;
        currentDampTime = _damptime;
        currentLookAtMouse = _lookAtMouse;
    }

    void SetCurrentValuesThirdPerson(float sensitivity, bool MMORPGCamera, bool invert) {
        currentMMORPGCamera = MMORPGCamera;
        currentInvert = invert;
        currentSensitivy = sensitivity;
    }

    public ControllerType GetControllerType() {
        return controllerType;
    }

    public bool GetMMORPGCamera() {
        return MMORPG_Camera;
    }
    #endregion
}
public enum ControllerType
{
    TopDown,
    ThirdPerson,
}
